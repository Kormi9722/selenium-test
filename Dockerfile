FROM python:3

WORKDIR /usr/src/app

RUN pip install selenium
RUN pip install pyvirtualdisplay
RUN pip install requests

COPY ./scriptother.py .

CMD [ "python", "./scriptother.py" ]
