#!groovy

//=================================================
// CONST
//=================================================

// APP NAME
final String APP_NAME = "tds"
final String JAR_FILE = "test-data-provider"

final String PROJECT_DIR = "wiq_testdataprovider"

// GIT REPO
final String GIT_REPOSITORY = "git.werkbliq.de/testing/testdataprovider.git"
final String GIT_REPOSITORY_URL = "https://${GIT_REPOSITORY}"


// REMOVE final String GIT_CONFIG_REPOSITORY = "git.werkbliq.de/backend/config-repository.git"  

// PORTS AUTH- und CONFIG-SERVICE
final CONFIGSERVICE_PORT = 10011
final String CONFIGSERVICE_URI = "https://local.werkbliq.de:${CONFIGSERVICE_PORT}"

// MAIL
final EMAIL_FROM = "jenkins@werkbliq.de"

// Build Version
final String DEFAULT_VERSION_QUALIFIER = "SNAPSHOT"

// credentials id
def gitCredentialsId = 'gitlab-credentials' 
def dockerhubCredentialsId = 'docker-hub-credentials'      // credentials for wiqregistry.azurecr.io
// REMOVE def dockerLiveRegistryCredentialsId = 'wiqliveregistry.azurecr.io'

// branch
def branch = "master"

// Build Version
def versionQualifier = DEFAULT_VERSION_QUALIFIER
def featureName = null


// docker registry
def dockerRepository = "wiqregistry.azurecr.io"

// REMOVE def dockerLiveRepository = "wiqliveregistry.azurecr.io"



// flags
def forceBuild = false
def skipTests = false
def skipDockerBuild = false
def skipDockerPush = false
def deployService = false
def skipSendMail = false
def createGitBranch = false



//=================================================
// PIPELINE
//=================================================


node {

    def version
    def versionNumber
    def needCheckout = true
    def lastBuildFailed = true

    try {

        // Stage: Init Variables
        //==============================================================================================================
        stage('Set Environment Variables') {

            // GIT
            //=============================================================
            if(env.GIT_CREDENTIALS_ID != null) {
                gitCredentialsId = env.GIT_CREDENTIALS_ID
            }
            echo "GIT_CREDENTIALS_ID: ${gitCredentialsId}"

            if(env.BRANCH != null) {
                branch = env.BRANCH
            }
            echo "BRANCH: ${branch}"



            // BUILD Version
            //=============================================================
            if(env.VERSION_QUALIFIER != null) {
                versionQualifier = env.VERSION_QUALIFIER
            }
            echo "VERSION_QUALIFIER: ${versionQualifier}"

            if (env.FEATURE_NAME != null) {
                featureName = env.FEATURE_NAME
            }
            echo "FEATURE_NAME: ${featureName}"


            // Docker
            //=============================================================
            if(env.DOCKER_HUB_CREDENTIALS_ID != null) {
                dockerhubCredentialsId = env.DOCKER_HUB_CREDENTIALS_ID
            }
            echo "DOCKER_HUB_CREDENTIALS_ID: ${dockerhubCredentialsId}"


            if(env.DOCKER_REPOSITORY != null) {
                dockerRepository = env.DOCKER_REPOSITORY
            }
            echo "DOCKER_REPOSITORY: ${dockerRepository}"


            // Database
            //==============================================================
            if(env.DATABASE_USERNAME != null) {
                databaseUser = env.DATABASE_USERNAME
            }
            echo "DATABASE_USERNAME: ${databaseUser}"


            // if(env.DOCKER_LIVE_REPOSITORY != null) {
            //     dockerLiveRepository = env.DOCKER_LIVE_REPOSITORY
            // }
            // echo "DOCKER_LIVE_REPOSITORY: ${dockerLiveRepository}"


            // Flags
            //=============================================================

            // force build
            if(env.FORCE_BUILD != null) {
                forceBuild = env.FORCE_BUILD == "true"
            }
            echo "FORCE_BUILD: ${forceBuild}"


            // skip tests
            if(env.SKIP_TESTS != null) {
                skipTests = env.SKIP_TESTS == "true"
            }
            echo "SKIP_TESTS: ${skipTests}"


            // docker build
            if(env.SKIP_DOCKER_BUILD != null) {
                skipDockerBuild = env.SKIP_DOCKER_BUILD == "true"
            }
            echo "SKIP_DOCKER_BUILD: ${skipDockerBuild}"


            // docker push
            if(env.SKIP_DOCKER_PUSH != null) {
                skipDockerPush = env.SKIP_DOCKER_PUSH == "true"
            }
            echo "SKIP_DOCKER_PUSH: ${skipDockerPush}"


            // deploy service
            if(env.DEPLOY_SERVICE != null) {
                deployService = env.DEPLOY_SERVICE == "true"
            }
            echo "DEPLOY_SERVICE: ${deployService}"
        }

        // Stages: Check Git Changes
        //==============================================================================================================
        stage('Check if Git has Changes') {

            withCredentials([usernamePassword(credentialsId: "${gitCredentialsId}", passwordVariable: 'GIT_PW', usernameVariable: 'GIT_USER')]) {

                dir("${PROJECT_DIR}") {
                    // checkout branch
                    checkoutBranch(GIT_USER, GIT_PW, GIT_REPOSITORY, branch)

                    // check if branch is up-to-date
                    needCheckout = gitNeedCheckout(GIT_USER, GIT_PW, GIT_REPOSITORY, branch)
                }
            }
        }

        // Return if no changes
        def doExit = !needCheckout && !forceBuild
        if(doExit) {
            currentBuild.result = "SUCCESS"
            echo "exit no changes in git"
            return
        }

        // Stage: Check Git Changes
        //==============================================================================================================
        stage('Checkout Branch') {
            echo "BRANCH: ${branch}"

            // checkout repository
            dir("${PROJECT_DIR}") {
                checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "*/${branch}"]], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: "${gitCredentialsId}", url: "${GIT_REPOSITORY_URL}"]]]


                withCredentials([usernamePassword(credentialsId: "${gitCredentialsId}", passwordVariable: 'GIT_PW', usernameVariable: 'GIT_USER')]) {
                    checkoutAndUpdateBranch(GIT_USER, GIT_PW, GIT_REPOSITORY, branch)
                }
            }

        }

        // Stage: Mvn Dependency List
        //==============================================================================================================
        stage('MVN Dep Tree') {
            dir("${PROJECT_DIR}") {
                bat 'mvn dependency:tree'
            }
        }

        stage('Database Testing Schema Restoration'){
            dir("${PROJECT_DIR}") {

                bat "sqlcmd /S wiq.database.windows.net,1433 /d weyellow -U HeitecOpsHumanAdmin -P 6ee4ecad-8821-475c-89cb-d657e263b222 -i .\\src\\main\\resources\\db\\migration\\0_CleanUp.sql"
                bat "sqlcmd /S wiq.database.windows.net,1433 /d weyellow -U HeitecOpsHumanAdmin -P 6ee4ecad-8821-475c-89cb-d657e263b222 -i .\\src\\main\\resources\\db\\migration\\1_Schema_Setup.sql"
                bat "sqlcmd /S wiq.database.windows.net,1433 /d weyellow -U HeitecOpsHumanAdmin -P 6ee4ecad-8821-475c-89cb-d657e263b222 -i .\\src\\main\\resources\\db\\migration\\2_Add_Foreign_Keys.sql"
                bat "sqlcmd /S wiq.database.windows.net,1433 /d weyellow -U HeitecOpsHumanAdmin -P 6ee4ecad-8821-475c-89cb-d657e263b222 -i .\\src\\main\\resources\\db\\migration\\3_Initial_Functions.sql"
                bat "sqlcmd /S wiq.database.windows.net,1433 /d weyellow -U HeitecOpsHumanAdmin -P 6ee4ecad-8821-475c-89cb-d657e263b222 -i .\\src\\main\\resources\\db\\migration\\4_Initial_Add_Check_Constraints.sql"
                bat "sqlcmd /S wiq.database.windows.net,1433 /d weyellow -U HeitecOpsHumanAdmin -P 6ee4ecad-8821-475c-89cb-d657e263b222 -i .\\src\\main\\resources\\db\\migration\\5_Create_Jointables.sql"
                bat "sqlcmd /S wiq.database.windows.net,1433 /d weyellow -U HeitecOpsHumanAdmin -P 6ee4ecad-8821-475c-89cb-d657e263b222 -i .\\src\\main\\resources\\db\\migration\\6_Create_Jointables.sql"
                bat "sqlcmd /S wiq.database.windows.net,1433 /d weyellow -U HeitecOpsHumanAdmin -P 6ee4ecad-8821-475c-89cb-d657e263b222 -i .\\src\\main\\resources\\db\\migration\\7_Add_Jointable_Constraints.sql"
                bat "sqlcmd /S wiq.database.windows.net,1433 /d weyellow -U HeitecOpsHumanAdmin -P 6ee4ecad-8821-475c-89cb-d657e263b222 -i .\\src\\main\\resources\\db\\migration\\8_Add_Jointable_Constraints.sql"
                bat "sqlcmd /S wiq.database.windows.net,1433 /d weyellow -U HeitecOpsHumanAdmin -P 6ee4ecad-8821-475c-89cb-d657e263b222 -i .\\src\\main\\resources\\db\\migration\\9_Create_Views.sql"
                bat "sqlcmd /S wiq.database.windows.net,1433 /d weyellow -U HeitecOpsHumanAdmin -P 6ee4ecad-8821-475c-89cb-d657e263b222 -i .\\src\\main\\resources\\db\\migration\\10_Initial_Inserts.sql"
                bat "sqlcmd /S wiq.database.windows.net,1433 /d weyellow -U HeitecOpsHumanAdmin -P 6ee4ecad-8821-475c-89cb-d657e263b222 -i .\\src\\main\\resources\\db\\migration\\11_Initial_Jt_Inserts.sql"

            }
        }


        // Stages: Testing
        //==============================================================================================================
        if(!skipTests) {



                // TESTS???



        }

        // Stage: Set Version Number in Maven POM
        //==============================================================================================================
        stage('Set Version Number') {
            dir("${PROJECT_DIR}") {
                // read version from json file

                version = readJSON file: 'version.json'    

                // set pom artifact version
                versionNumber = setAndReturnMavenPomVersion(version, versionQualifier, featureName)

                // set pom config label
                if(!createGitBranch) {
                    setGitConfigLabelInMavenPom(branch)
                }
                else {
                    def featureBranch = buildBranchName(version, versionQualifier, featureName)
                    setGitConfigLabelInMavenPom(featureBranch)
                }
            }
        }


        // Stage: Build Jar
        //==============================================================================================================
        stage('Build Java Artifact') {
            dir("${PROJECT_DIR}") {
                withEnv(["JAVA_HOME=${env.JAVA_HOME11}"]) {
                    bat 'mvn clean package -U -DskipTests=true -Ddockerfile.skip'
                }
            }
        }


        // Stage: Build Docker Image
        //==============================================================================================================
        if(!skipDockerBuild) {

            if(versionQualifier == "SNAPSHOT") {
                stage('Build Docker Image') {
                    dir("${PROJECT_DIR}") {
                        withCredentials([usernamePassword(credentialsId: "${databaseUser}", passwordVariable: 'DATABASE_PW', usernameVariable: 'DATABASE_USER')]) {
                            bat "docker build -t ${dockerRepository}/${APP_NAME}:${versionNumber} -t ${dockerRepository}/${APP_NAME}:latest --build-arg JAR_FILE=${JAR_FILE}-${versionNumber}.jar  --build-arg VERSION_TAG=${versionNumber} --build-arg USERNAME_ARG=${DATABASE_USER} --build-arg PASSWORD_ARG=${DATABASE_PW} ."
                        }
                    }
                }
            }
        }


        // Stage: Push Docker Image to Registry
        //==============================================================================================================
        if(!skipDockerPush) {

            if(versionQualifier == "SNAPSHOT") {
                stage('Push Docker Image to Registry') {
                    dir("${PROJECT_DIR}") {
                        withCredentials([usernamePassword(credentialsId: "${dockerhubCredentialsId}", passwordVariable: 'DOCKER_PW', usernameVariable: 'DOCKER_USER')]) {
                            DOCKER_USER = DOCKER_USER.replace("@", "%%40")

                            // docker login
                            bat "docker login --username ${DOCKER_USER} --password ${DOCKER_PW} ${dockerRepository}"

                            // docker push image
                            bat "docker push ${dockerRepository}/${APP_NAME}:${versionNumber}"

                            // don't tag images from a feature branch with "latest"
                            // if(featureName == null || featureName == "")
                            //     bat "docker push ${dockerRepository}/${APP_NAME}:latest"
                        }
                    }
                }
            }
        }

        // Stage: Commit Version Number
        //==============================================================================================================
        stage('Commit Version Number') {
            dir("${PROJECT_DIR}") {
                // set SNAPSHOT Version in Maven POM
                setAndReturnMavenPomVersion(version, DEFAULT_VERSION_QUALIFIER, featureName)

                // commit changes
                withCredentials([usernamePassword(credentialsId: "${gitCredentialsId}", passwordVariable: 'GIT_PW', usernameVariable: 'GIT_USER')]) {
                    GIT_USER = GIT_USER.replace("@", "%%40")
                    bat "git pull https://${GIT_USER}:${GIT_PW}@${GIT_REPOSITORY} ${branch}"

                    def message = "set Version ${versionNumber}"
                    commitChanges(GIT_PW, GIT_PW, GIT_REPOSITORY, message)
                }
            }
        }

        currentBuild.result = "SUCCESS"

    } catch (e) {
        currentBuild.result = "FAILURE"
        throw e

    } finally {

        // Stage: Clean up
        //==============================================================================================================
        stage('Clean up') {
            dir("${PROJECT_DIR}") {
                bat 'mvn clean || exit 0'
            }
        }

        // Stage: Send Statue Email
        //==============================================================================================================
        if(skipSendMail) {
            stage('Send Email') {
                // send mail
                // sendMail(EMAIL_FROM)
            }
        }
        echo "Finish"
    }
}


//======================================================================================================================
// Functions
//======================================================================================================================




// Function: wait for service
//======================================================================================================================
def waitForService(SERVICE_URI) {
    echo "Wait for service: ${SERVICE_URI}"
    def status
    def result
    def errorCode = 1
    def cmd = 'curl  -o NUL -k -s  -w %%{http_code}\\n ' + " ${SERVICE_URI} || exit 0 "

    echo  "cmd: ${cmd} "
    for (int i = 0; i < 100; i++) {
        echo "retry ${i}:"
        status = bat(returnStdout: true, script:  cmd ).trim()
        echo "status: ${status}"
        //erste Zeile entfernen, da steht der Aufruf drin...
        result = status.readLines().drop(1).join(" ")
        if (result.toInteger() == 200) {
            errorCode = 0
            break
        }
        sleep 3
    }
    if (errorCode == 1) {
        error "config-service does not running"
    }
}


// Function: build version number
//======================================================================================================================

def String buildVersionNumber(major, minor, qualifier, featureName) {

    def optionalFeatureName = (featureName == null || featureName == "") ? "" : "-" + featureName

    if (qualifier == "SNAPSHOT") {
        return "${major}.${minor}${optionalFeatureName}-${qualifier}"
    }

    def timeStamp = Calendar.getInstance().getTime().format('YYYYMMddHHmmss', TimeZone.getTimeZone('CET'))

    if (qualifier == "") {
        return "${major}.${minor}.${BUILD_NUMBER}${optionalFeatureName}-${timeStamp}"
    }

    return "${major}.${minor}.${BUILD_NUMBER}${optionalFeatureName}-${qualifier}-${timeStamp}"
}


// Function: build branch name
//======================================================================================================================
def String buildBranchName(version, qualifier, featureName) {

    if(featureName == null || featureName == "")
        return  "release/" + version.major + "." + version.minor
    else
        return "feature/" + featureName
}


// Function: escape branch for Config-Service
//======================================================================================================================

def String escapeSlashesInBranchName(name) {
    return name.replaceAll('/', '(_)')
}


// Function: git clone repo
//======================================================================================================================

def clone(gitUser, gitPassword, gitRepo, branch) {

    gitUser = gitUser.replace("@", "%%40")

    bat "git clone -b ${branch} https://${gitUser}:${gitPassword}@${gitRepo}"
}


// Function: git tag
//======================================================================================================================

def gitTag(gitUser, gitPassword, gitRepo, tag) {

    gitUser = gitUser.replace("@", "%%40")

    bat "git tag ${tag}"
    bat "git push https://${gitUser}:${gitPassword}@${gitRepo} --force --tags"
}

// Function: check if git remote branch has changes
//======================================================================================================================

def gitRemoteBranchHasChanges(gitUser, gitPassword, gitRepo, branch) {

    gitUser = gitUser.replace("@", "%%40")

    bat "git fetch https://${gitUser}:${gitPassword}@${gitRepo} +refs/heads/*:refs/remotes/origin/* || exit 0"

    def remote = bat(returnStdout: true, script: "git rev-parse origin/${branch} || exit 0").trim().readLines().drop(1).join(" ")
    echo "remote hash: ${remote}"

    def local = bat(returnStdout: true, script: "git rev-parse ${branch} || exit 0").trim().readLines().drop(1).join(" ")
    echo "local hash: ${local}"

    def hasChanges = remote == null || remote == "" || remote != local

    return hasChanges;
}

// Function: check need checkout
//======================================================================================================================

def gitNeedCheckout(gitUser, gitPassword, gitRepo, branch) {

    gitUser = gitUser.replace("@", "%%40")

    def hasChanges = gitRemoteBranchHasChanges(gitUser, gitPassword, gitRepo, branch)

    def lastBuildFailed = !hudson.model.Result.SUCCESS.equals(currentBuild.rawBuild.getPreviousBuild()?.getResult())
    def prevNumber = currentBuild.rawBuild.getPreviousBuild()?.number
    echo "previous Build Number: ${prevNumber}"
    echo "last Build Failed: ${lastBuildFailed}"

    def needCheckout = hasChanges || lastBuildFailed;
    echo "needCheckout: ${needCheckout}"

    return needCheckout;
}

// Function: check if git branch exists
//======================================================================================================================

def Boolean checkBranchExists(gitUser, gitPassword, gitRepo, branch) {

    gitUser = gitUser.replace("@", "%%40")

    def checkBranchExistsCmd = "git ls-remote --exit-code https://${gitUser}:${gitPassword}@${gitRepo} --heads origin refs/heads/${branch}"
    def branchExist = bat(returnStatus : true, script:  checkBranchExistsCmd ) == 0
    echo "check branch ${branch} exists: ${branchExist}"

    return branchExist
}

// Function: create git branch
//======================================================================================================================

def createBranch(gitUser, gitPassword, gitRepo, branch) {

    gitUser = gitUser.replace("@", "%%40")

    def branchExits = checkBranchExists(gitUser, gitPassword, gitRepo, branch)
    if(branchExits)
        return

    echo "create branch: ${branch}"

    // create branch
    bat "git branch ${branch}"

    // push branch
    bat "git push https://${gitUser}:${gitPassword}@${gitRepo} ${branch}"
}



// Function: checkout git branch
//======================================================================================================================

def checkoutAndUpdateBranch(gitUser, gitPassword, gitRepo, branch) {

    gitUser = gitUser.replace("@", "%%40")

    checkoutBranch(gitUser, gitPassword, gitRepo, branch)

    // update branch
    bat "git pull https://${gitUser}:${gitPassword}@${gitRepo} ${branch} || exit 0"
}

// Function: checkout git branch
//======================================================================================================================

def checkoutBranch(gitUser, gitPassword, gitRepo, branch) {

    // branch is headless after jenkins plugin checkout (detached HEAD)
    bat "git checkout ${branch} || git checkout -b ${branch} origin/${branch} || exit 0"

}


// Function: commit git changes
//======================================================================================================================

def commitChanges(gitUser, gitPassword, gitRepo, message) {

    gitUser = gitUser.replace("@", "%%40")

    bat "git commit -a -m \"Jenkins Build ${BUILD_NUMBER}: ${message}\" & exit 0"
    bat "git push https://${gitUser}:${gitPassword}@${gitRepo}"
}

// Function: set Maven Pom Version
//======================================================================================================================

def String setAndReturnMavenPomVersion(version, versionQualifier, featureName) {

    // build version number
    versionNumber = buildVersionNumber(version.major, version.minor, versionQualifier, featureName)
    echo "Version Number: ${versionNumber}"

    // set Version in Maven POM
    bat "mvn versions:set -DnewVersion=${versionNumber}"

    return versionNumber
}

// Function: set Git-Branch in POM
//======================================================================================================================

def setGitConfigLabelInMavenPom(branch) {

    def escapedBranchName = escapeSlashesInBranchName(branch)
    echo "Escaped Branch Name in Pom: ${branch}"
    bat "mvn versions:set-property -Dproperty=docker.run.sh.config-label -DnewVersion=${escapedBranchName}"
}



// Function: send status email
//======================================================================================================================
def sendMail(from) {

    def lastBuildFailed = !hudson.model.Result.SUCCESS.equals(currentBuild.rawBuild.getPreviousBuild()?.getResult())
    def currentBuildFailed = currentBuild.result != "SUCCESS"
    def notAborted = currentBuild.result != "ABORTED"

    echo "BUILD RESULT: ${currentBuild.result}"

    if(currentBuildFailed && notAborted) {

        emailext attachLog: true, body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:

Check console output at $BUILD_URL to view the results.


------------------------
Changes
------------------------
${CHANGES_SINCE_LAST_SUCCESS, reverse=true}


------------------------
Tests Failed
------------------------
${BUILD_LOG_MULTILINE_REGEX, regex="Running .*[\\\\r\\\\n]+Tests .*<<< FAILURE!((?!Running|Results)(.*[\\\\r\\\\n]))+", maxMatches=20, showTruncatedLines=false, escapeHtml=false}


------------------------
Errors
------------------------
${BUILD_LOG_REGEX, regex="\\\\[ERROR\\\\]\\s*.*", linesBefore=0, linesAfter=0, maxMatches=20, showTruncatedLines=false, escapeHtml=false}


------------------------
Log 
------------------------
${BUILD_LOG, maxLines=300, escapeHtml=false}

''', subject: 'Jenkins Build: $PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!', to: '$DEFAULT_RECIPIENTS', from: "${from}"

    }
    else if(lastBuildFailed) {
        emailext attachLog: true, body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:

Check console output at $BUILD_URL to view the results.

''', subject: 'Jenkins Build: $PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!', to: '$DEFAULT_RECIPIENTS', from: "${from}"
    }

}
