from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time

print("A")
desiredCapabilities = DesiredCapabilities.CHROME.copy()
time.sleep(5)
print("B")
chromeOptionsRemote = webdriver.ChromeOptions()
time.sleep(5)
print("C")
chromeOptionsRemote.add_argument('--headless')
chromeOptionsRemote.add_argument('--no-sandbox')
chromeOptionsRemote.add_argument('--disable-dev-shm-usage')
time.sleep(5)
print("E")

remdriver = webdriver.Remote(options=chromeOptionsRemote, command_executor="http://10.0.76.54:4444/wd/hub", desired_capabilities=desiredCapabilities)

print("F")
time.sleep(5)
print("G")
remdriver.close()
remdriver.quit()
print("H")